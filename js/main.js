$( function() {

    // Делаем префикс js__ для всех кнопок которые требуют события в JS (что бы при редактировании шаблона человек знал что на этом классе что-то подвязано в js)
    // Ну кроме overlay - думаю это для всех понятно.
    let mobileMenu =      $('.js__mobile-menu'),
        mobileMenuClose = $('.js__mobile-menu-close'),
        mobileMenuNav   = $('.js__navigation'),
        overlay         = $('.overlay'),
        editLink        = $('.js__edit-field'),
        inputTextField  = $('.js__input-text'),
        goToLink        = $('.js__go-to');


    function openMenu() {
        mobileMenu.addClass('opened');
        overlay.fadeIn();
        mobileMenuNav.addClass('opened');
    }

    function closeMenu() {
        mobileMenu.removeClass('opened');
        overlay.fadeOut();
        mobileMenuNav.removeClass('opened');
    }

    function goTo(pageID) {
        $("html, body").animate({
            scrollTop: $('#'+pageID).offset().top - 90
        }, 600);
        if ($(document).width() < 768) {
            closeMenu();
        }
    }

    // open menu
    mobileMenu.on('click', function () {
        openMenu();
    });

    // click on overlay
    overlay.on('click', function () {
        closeMenu()
    });

    // click on close icon
    mobileMenuClose.on('click', function () {
        closeMenu()
    });

    // when click on edit - focus on input
    editLink.on('click', function () {
        $(this).prev('input').prop('disabled',false);
        $(this).prev('input').focus();
    });

    // return disabled when focus out
    inputTextField.focusout(function () {
        $(this).prop('disabled',true);
    });

    // go to
    goToLink.on('click', function (e) {
        e.preventDefault();
        var pageID = $(this).data('pageid');
        goTo(pageID);
    });

    // date picker
    $( "#datepicker" ).datepicker();
});